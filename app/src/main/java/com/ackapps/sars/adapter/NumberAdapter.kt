package com.ackapps.sars.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ackapps.sars.R
import com.ackapps.sars.databinding.ItemNumberBinding

class NumberAdapter(numbers: List<Pair<String,String>>, itemOnClickListener: OnClickCallback) : RecyclerView.Adapter<NumberAdapter.ViewHolder>() {

    private var numberList: List<Pair<String,String>> = listOf()
    private var itemOnClickListener: OnClickCallback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemNumberBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_number, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item =  numberList[position]

        holder.binding.number = item.first


        // Tıklandığında telefon numarasını gönderir
        holder.binding.numberCardView.setOnClickListener {
            itemOnClickListener.onClick(item.second)
        }

    }
    override fun getItemCount(): Int {
        return numberList.size
    }

    inner class ViewHolder(itemView: ItemNumberBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        var binding: ItemNumberBinding = itemView
    }

    init {
        this.numberList = numbers
        this.itemOnClickListener = itemOnClickListener
    }
}