package com.ackapps.sars.adapter

interface OnClickCallback {
    fun onClick(phoneNumber : String)
}