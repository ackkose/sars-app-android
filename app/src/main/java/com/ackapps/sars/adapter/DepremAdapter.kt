package com.ackapps.sars.adapter


import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ackapps.sars.model.Deprem
import com.ackapps.sars.R
import java.util.*

class DepremAdapter(val deprem_List: ArrayList<Deprem>) :
        RecyclerView.Adapter<DepremAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val inflater =
                LayoutInflater.from(parent.context).inflate(R.layout.item_deprem, parent, false)

        return ViewHolder(inflater)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.txt_tarih.text= deprem_List[position].tarih + " / " + deprem_List[position].saat
        holder.txt_yer.text= deprem_List[position].yer
        holder.txt_siddet.text= deprem_List[position].buyukluk
        holder.txt_derinlik.text= deprem_List[position].derinlik
        try{
            val color = when(deprem_List[position].buyukluk!!.toDouble()){
                in 0.0 .. 2.0 -> {
                    R.color.green_primary_dark
                }
                in 2.0 .. 2.5 -> {
                    R.color.orange_primary
                }
                in 2.5 .. 3.0 -> {
                    R.color.deep_orange_primary_dark
                }
                in 3.0 .. 10.0 -> {
                    R.color.red_nature
                }
                else -> R.color.green_primary
            }

                holder.masterLay.setBackgroundColor(ContextCompat.getColor(holder.masterLay.context,color))
        }catch (e:Exception) {
            e.printStackTrace()
            holder.masterLay.setBackgroundColor(ContextCompat.getColor(holder.masterLay.context,R.color.gray))
        }

    }

    override fun getItemCount(): Int {

        return deprem_List.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val txt_tarih = itemView.findViewById<TextView>(R.id.txttarih)
        val txt_yer = itemView.findViewById<TextView>(R.id.txtyer)
        val txt_siddet = itemView.findViewById<TextView>(R.id.txtsiddet)
        val txt_derinlik = itemView.findViewById<TextView>(R.id.txtderinlik)
        val masterLay = itemView.findViewById<LinearLayout>(R.id.masterLay)


    }


}