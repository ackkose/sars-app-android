package com.ackapps.sars.service


import com.ackapps.sars.model.Deprem
import retrofit2.Call
import retrofit2.http.GET

interface Api {


    @GET("api")
    fun getData(): Call<ArrayList<Deprem>>
}