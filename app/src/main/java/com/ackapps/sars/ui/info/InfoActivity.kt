package com.ackapps.sars.ui.info

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.ackapps.sars.R

class InfoActivity : AppCompatActivity() {

    lateinit var infoTextView:TextView
    var isClick = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        infoTextView = findViewById(R.id.textDepremInfo)

        infoTextView.setOnClickListener {
            if(isClick){
                infoTextView.text ="AÇIK ALANDAYSANIZ;\n" +
                        "Enerji hatları ve direklerinden, ağaçlardan, diğer binalardan ve duvar diplerinden uzaklaşılmalıdır. Açık arazide çömelerek etraftan gelen tehlikelere karşı hazırlıklı olunmalıdır.\n" +
                        "Toprak kayması olabilecek, taş veya kaya düşebilecek yamaç altlarında bulunulmamalıdır. Böyle bir ortamda bulunuluyorsa seri şekilde güvenli bir ortama geçilmelidir.\n" +
                        "Binalardan düşebilecek baca, cam kırıkları ve sıvalara karşı tedbirli olunmalıdır.\n" +
                        "Toprak altındaki kanalizasyon, elektrik ve gaz hatlarından gelecek tehlikelere karşı dikkatli olunmalıdır.\n" +
                        "Deniz kıyısından uzaklaşılmalıdır."
            }else{
                infoTextView.text = getString(R.string.deprem_info)
            }
            isClick = !isClick
        }
    }


}