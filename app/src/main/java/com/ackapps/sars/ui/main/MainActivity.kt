package com.ackapps.sars.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.ackapps.sars.R

import com.ackapps.sars.adapter.DepremAdapter

import com.ackapps.sars.model.Deprem
import com.ackapps.sars.service.ApiService
import retrofit2.Call
import retrofit2.Response



class MainActivity : AppCompatActivity() {

    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    lateinit var rv_deprem: RecyclerView
    lateinit var progressBar: ProgressBar
    private val depremApiService = ApiService()
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        rv_deprem = findViewById(R.id.rvDeprem)
        progressBar = findViewById(R.id.progressBar)
        swipeRefreshLayout = findViewById(R.id.swipeRefresh)
        rv_deprem.layoutManager = LinearLayoutManager(this)

        get_Data()

        swipeRefreshLayout.setOnRefreshListener {
            progressBar.visibility = View.VISIBLE
            rv_deprem.visibility = View.GONE
            get_Data()
        }


    }

    fun get_Data() {


        val result = depremApiService.getData()

        result.enqueue(object : retrofit2.Callback<ArrayList<Deprem>> {
            override fun onResponse(
                    call: Call<ArrayList<Deprem>>,
                    response: Response<ArrayList<Deprem>>
            ) {

                val item = response.body()

                var list_Deprem: ArrayList<Deprem> = ArrayList()

                item?.let {
                    for (i in 0 until item.size) {

                        var deprem = Deprem(
                                item[i].tarih,
                                item[i].saat,
                                item[i].enlem,
                                item[i].boylam,
                                item[i].derinlik,
                                item[i].buyukluk,
                                item[i].yer,
                                item[i].sehir
                        )

                        list_Deprem.add(deprem)


                    }

                }

                rv_deprem.adapter = DepremAdapter(list_Deprem)
                progressBar.visibility = View.GONE
                rv_deprem.visibility = View.VISIBLE
                swipeRefreshLayout.isRefreshing = false


            }

            override fun onFailure(call: Call<ArrayList<Deprem>>, t: Throwable) {
                Log.d("test1", t.toString())

            }

        })

    }
}