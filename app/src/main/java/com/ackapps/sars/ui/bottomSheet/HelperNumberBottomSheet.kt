package com.ackapps.sars.ui.bottomSheet

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import com.ackapps.sars.R
import com.ackapps.sars.adapter.NumberAdapter
import com.ackapps.sars.adapter.OnClickCallback
import com.ackapps.sars.databinding.ActivityNumberBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class  HelperNumberBottomSheet : BottomSheetDialogFragment() {

    lateinit var binding : ActivityNumberBinding
    val numberList = mutableListOf<Pair<String,String>>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        // Binding Kurulumu - Örnek
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.activity_number,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Pair -> iki değeri birbirine bağlayan bir java veri türü
        numberList.add(Pair("Jandarma","156"))
        numberList.add(Pair("Polis","155"))
        numberList.add(Pair("Acil Servis","112"))
        numberList.add(Pair("İtfaiye","110"))
        numberList.add(Pair("Sağlık Bakanlığı","184"))
        numberList.add(Pair("Alo AFAD","122"))

        binding.helperNumberRV.adapter = NumberAdapter(
            // Numara Listesi
            numberList,

            // Tıklama interface ataması
            object : OnClickCallback {
                override fun onClick(phoneNumber: String) {
                    val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phoneNumber"))
                    startActivity(intent)
                }
            }
        )
    }

    //singleton
    companion object {
        fun showNumberBottomSheet(fragmentManager: FragmentManager): HelperNumberBottomSheet {
            val numberBottomString = HelperNumberBottomSheet()
            numberBottomString.isCancelable = true

            if (!numberBottomString.isAdded) {
                numberBottomString.show(fragmentManager, numberBottomString::class.java.name)
            }

            return numberBottomString
        }
    }

}