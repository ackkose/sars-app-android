package com.ackapps.sars.ui.login
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ackapps.sars.R
import com.ackapps.sars.ui.bottomSheet.HelperNumberBottomSheet
import com.ackapps.sars.ui.info.InfoActivity
import com.ackapps.sars.ui.main.MainActivity
import com.bumptech.glide.Glide


class LoginActivity : AppCompatActivity() {

    lateinit var logoIV:ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        logoIV = findViewById(R.id.logoIV)
       /* Glide.with(this)
            .load(R.drawable.ic_sars_logo)
            .into(logoIV)*/
    }

    fun goMain(view: View) {
        showToast("main")
        startMainActivity()
    }
    fun goDeprem(view:View) {
        showToast("deprem")
        startDepremInfoActivity()
    }
    fun goAny(view:View) {
        showToast("any")
        showNumberBottomSheet()
    }

    fun startMainActivity(){
        startActivity(Intent(this, MainActivity::class.java))
    }

    fun startDepremInfoActivity(){
        startActivity(Intent(this, InfoActivity::class.java))
    }

    fun showNumberBottomSheet(){
        HelperNumberBottomSheet.showNumberBottomSheet(supportFragmentManager)
    }


    fun showToast(message:String){
        //Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }
}